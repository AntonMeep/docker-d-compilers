FROM debian:stretch-slim

ENV COMPILER=ldc             \
    COMPILER_VERSION=1.15.0

RUN echo ">>> Making apt non-interactive <<<"                                  \
 && export DEBIAN_FRONTEND=noninteractive                                      \
 && echo 'APT::Get::Assume-Yes "true";'                                        \
      >> /etc/apt/apt.conf.d/99-ldc                                            \
 && echo 'DPkg::Options "--force-confnew";'                                    \
      >> /etc/apt/apt.conf.d/99-ldc                                            \
 && echo ">>> Updating <<<"                                                    \
 && apt-get update -qq                                                         \
 && echo ">>> Installing necessary dependencies <<<"                           \
 && apt-get install -y --no-install-recommends libssl1.1 libssl-dev            \
      curl xz-utils libc-dev gnupg gcc ca-certificates unzip                   \
      libcurl4-openssl-dev zlib1g-dev libxml2 2>&1 > /dev/null                 \
 && echo ">>> Installing ${COMPILER} ${COMPILER_VERSION} <<<"                  \
 && mkdir /dlang /source                                                       \
 && curl -fsS https://dlang.org/install.sh                                     \
      | bash -s install -p /dlang -a "${COMPILER}-${COMPILER_VERSION}"         \
 && chmod -R 755 /dlang                                                        \
 && echo ">>> Removing unnecessary dependencies <<<"                           \
 && apt-get purge --auto-remove -yqq curl gnupg xz-utils unzip                 \
      2>&1 > /dev/null                                                         \
 && echo ">>> Cleaning up <<<"                                                 \
 && rm -rf /var/lib/apt/lists /var/cache/apt/archives                          \
 && rm -rf /etc/apt/apt.conf.d/99-ldc                                          \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/lib32                        \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/activate                     \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/activate.fish                \
 && rm -rf /dlang/${COMPILER}-${COMPILER_VERSION}/README

ENV \
  PATH=/dlang/${COMPILER}-${COMPILER_VERSION}/bin:${PATH} \
  LD_LIBRARY_PATH=/dlang/${COMPILER}-${COMPILER_VERSION}/lib \
  LIBRARY_PATH=/dlang/${COMPILER}-${COMPILER_VERSION}/lib

VOLUME /source
WORKDIR /source
